﻿using System;
using System.IO;
using UnityEngine;

namespace JamuMamu
{
    /// <summary>
    /// Implements a plain old serializable class where you can stick anything JsonUtility can read and write.
    /// Could almost certainly implement this more elegantly!
    /// </summary>
    [Serializable]
    public class SaveGame
    {
        public Color CubeColor;

        public static SaveGame Load()
        {
            string filePath = GetSavePath();
            if (File.Exists(filePath))
            {
                string saveData = File.ReadAllText(filePath);
                return JsonUtility.FromJson<SaveGame>(saveData);
            }
            else
            {
                return new SaveGame();
            }
        }

        private static string GetSavePath()
        {
#if !UNITY_EDITOR && UNITY_WEBGL
            return "/idbfs/JamuMamuCoolSaveZone";
#else
            return Path.Combine(Application.persistentDataPath, "CoolSaveData");
#endif
        }

        public void Save()
        {
            string saveData = JsonUtility.ToJson(this);
            File.WriteAllText(GetSavePath(), saveData);

#if !UNITY_EDITOR && UNITY_WEBGL
            // This bit of magic makes sure the virtual filesystem is flushed to the IndexedDB
            // Really this should be implemented as an external function and linked here!
            Application.ExternalEval("FS.syncfs(false, function (err) {})");
#endif
        }
    }
}
