﻿using UnityEngine;

namespace JamuMamu
{
    public class Spin : MonoBehaviour
    {
        private Color color;
        private Material material;

        SaveGame saveGame;

        void Start()
        {
            material = GetComponent<MeshRenderer>().material;

            saveGame = SaveGame.Load();
            material.color = saveGame.CubeColor;
        }

        void Update()
        {
            transform.rotation = Quaternion.AngleAxis(Time.time * 4f, Vector3.up);

            if (Input.anyKeyDown)
            {
                SetRandomColor();
            }
        }

        private void SetRandomColor()
        {
            color = Color.HSVToRGB(Random.value, 1, 1);
            material.color = color;

            saveGame.CubeColor = color;
            saveGame.Save();
        }
    }
}
